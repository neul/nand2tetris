

# A Instruction:
    + includes "@"value

    + Where value is either:
        + a non-negative decimal constant 
            + @21 = 21 in 15 bit binary  constant
            Constants: May appear only in A-instructions of the form @xxx. The
            constant xxx is a value in the range 0–32767 and is written in decimal notation.
        + a symbol reffering to such a constant
            + @A = symbol or variable "A"
    + binary syntax
        + 0+15 bit
    0 - 32767

# C Instruction:
    + dest = comp ; jump
    + syntax is inside of the book
    + i need to create map table for three different fields
        + destionation table
        + computation table 
        + jump table
    + binary syntax 
        + opcode: 1 1 1 
        + comp: a c1 c2 c3 c4 c5 c6 
        + destionation: d1 d2 d3 
        + jump: j1 j2 j3

    + comp is mandatory
    + if destination field is empty, = is omitted i.e ignored
    + if jump is empty ; is ignored

# Symbols:
    + Symbols: A symbol can be any sequence of letters, digits, underscore (_),
    dot (.), dollar sign ($), and colon (:) that does not begin with a digit.

## All the assembly mnemonics (like JEQ, and so on) must be written in uppercase.

    + Predefined: -- A map table for this
        + R0 - R15
        + Screen = 16384
        + KBD = 24576
        + SP = 0
        + LCL = 1 
        + ARG = 2 
        + THIS = 3
        + THAT = 4
    + User defined symbols: -- Another map table for this
        + Labels:
            + Defines as (XXX)
            + Corresponds to line number+1
            + Don't count labels as instructions
        + Variables: 
            + First variable mapped to RAM[16]
            + If you see for the first time, assign a unique memory adressAA
            + replace with value

# White space
    + Empty lines / indentation
    + Line comments //
    + In-line comment //

# Functions:
    enum A or C: whichInstruction(char* line) if @ A else C
        if line starts with 0 then A else C
    isEmptyLine(char* line)

# Tables
    + Predefined mnemonics table
    + Symbol table at the first pass

# Initialization:
    + Construct an empty symbol table 
    + Add the pre-defined symbols to the symbol table
# First Pass:
    keeping track of the line number, the number starts at 0 and is incremented by 1 
    whenever an *A-instruction* or a *C-instruction* is encountered. 
    Each time a label declaration (xxx) is encountered, the
    assembler adds a new entry to the symbol table, associating the symbol xxx
    with the current line number plus 1 (this will be the ROM address of the next instruction in the program).
    This pass results in adding to the symbol table all the program’s label
    symbols, along with their corresponding values.
    + look just for the label decorations (
    + keep track of how many lines you have

# Second Pass;
    + set n to 16
    if the instruction is @symbol, look up in the symbol table:
        if found, use value to complete the instruction's translation
        if not:
            add symbol, n to the symbol table 
            n++
    + add variables to the symbol table
    

# Tasks:
    + Reading and Parsing Commands:
        + Start to reading a file with a given name [x]
          void parser(char* file_name); 
        + Move to the next command in the file
            bool hasMoreCommands?
            need to skip whitespace including comments
        + Get the fileds of the current command:
            Type of command (A or C or Label)
            need to split based on type of command, comp, dest, jump

    + Translating Mnemonic to Code: 
        + For every different type of commands, they need their own tables
        + Need to have dest table and corresponding values

        char* dest(char* cmd); char* comp(char* cmd); char* jump(char* cmd);
        returns binary corresponded code

    + Using the symbol table 
        + Create a new table 
        + Add all predefined symbols 
        + While reading input add labels and new variables to the table 
            + Labels: when you see a "(xxx)" command ad xxx and the addres sof the next machine language command
            + requires maintaing this this running address, this may need to be done in a first pass
            + do it in the first pass 
        + Whenever you see a "@xxx" command, where xx is not a number, consult the table to replace the symbol
        + and if not in the table add "xxx" and free n

# Overall Logic:
## Init:
    + Functions 
    + Symbol table
## First pass:
    read all command, only focus on labels
# Restart reading and translating commands 
# Main Loop:
    + Get the next assembly lang command and parse it (decide if a or c and split it)
    + For A-Com: Translate symbols to binary addresses
    + For C-Com: get code for each part and put them together
    + output the resulting machine lang command (write it)

Parser: Unpacks each instruction into into its underlying fields 
    split
    enum whichCmdType
Code: Translates each filed into its corresponding binary value 
Table: HashTable

XXx.asm to Xxx.hack
./Assembler filename 
if file exists overwrite
