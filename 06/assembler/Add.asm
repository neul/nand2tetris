// Computes R0 = 2 + 3  (R0 refers to RAM[0])
// https://inv.vern.cc/watch?v=9Sq94l9fYqw&list=PLrDd_kMiAuNmSb-CKWQqq9oBFN_KNMTaI&index=41
@23
D=A
@3
D=D+A
@0
M=D
