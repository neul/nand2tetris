#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct hash_node {
	char* key;
	char* value;
	struct hash_node* next;
	struct hash_node* previous;
} hash_node;

typedef struct hash_table {
	int size;
    int count;
	hash_node** table;
} hash_table;

hash_node* create_hash_node(char* key, void* value);
hash_table* create_hash_table();
int hash(char* key, int ht_size);
void add(hash_table* ht, char* key, void* value);
bool exists(hash_table* ht, char* key);
hash_node* get(hash_table* ht, char* key);
void remove_item(hash_table* ht, char* key);
