#ifndef REGISTERS_H
#include "hash.h"

typedef enum {
    C,
    A,
    Label
} TokenTypes;

typedef struct {
    char* token;
    TokenTypes token_type;
    int len;
} TokenLine;

hash_table* create_symbol_table();
hash_table* create_jump_table();
hash_table* create_dest_table();
hash_table* create_comp_table();

TokenTypes* whichType(char* token);
TokenLine* create_token_struct();

int parse_file(char* filename, hash_table* symbol_table);
TokenLine* parse(char* line, hash_table* symbol_table);
bool is_valid_filename(char* filename);
bool is_file_exits(char* filename);
bool is_line_comment(char* line);
bool is_line_empty(char* line);
int change_char_to_int(char* charNum);
int place_value(int place);


#endif
