#include "hash.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>

hash_node* create_hash_node(char* key, void* value) {
	hash_node* hd = malloc(sizeof(hash_node));
	hd->key = strdup(key);
	hd->value = strdup(value);
	hd->next = NULL;
	hd->previous = NULL;
	return hd;
}

hash_table* create_hash_table() {
	hash_table* ht = malloc(sizeof(hash_table));
	ht->size = 53;
    ht->count = 0;
	ht->table = calloc(53, sizeof(hash_node*));
	return ht;
}

int hash(char* key, int ht_size) {
    int sumOfAscii = 0;
    for (int i = 0; i < strlen(key); i++) sumOfAscii+= key[i];
	return sumOfAscii % ht_size;
}

void add(hash_table* ht, char* key, void* value) {
    int index = hash(key, ht->size);
    if (exists(ht, key) && strcmp(ht->table[index]->key, key) == 0) {
        printf("Already exists\n");
        return;
    }

    hash_node* node = create_hash_node(key, value);

    if (!exists(ht, key) && ht->table[index] == NULL) {
        ht->table[index] = node;
    } else {
        hash_node* current_node = ht->table[index];
        while (current_node->next != NULL) current_node = current_node->next;
        current_node->next = node;
        node->previous = current_node;
    }
    ht->count++;
}

bool exists(hash_table* ht, char* key) {
    int index = hash(key, ht->size);
    hash_node* node = ht->table[index];

    while (node != NULL) {
        if (strcmp(node->key, key) == 0) return true;
        node = node->next;
    }
    return false;
}

hash_node* get(hash_table* ht, char* key) {
    hash_node* node;
    if (!exists(ht, key)) {
        printf("Not exists!\n");
        return NULL;
    }
    node = ht->table[hash(key, ht->size)];
    while (node != NULL && strcmp(node->key, key) != 0) node = node->next;
    return node;
}


void remove_item(hash_table* ht, char* key) {
    if (!exists(ht, key)) {
        printf("Not exists!\n");
        return;
    }
    
    hash_node* node = get(ht, key);
    int index = hash(key, ht->size);

    if (node != NULL) {
        if (node->next != NULL && node->previous != NULL) {
            node->previous->next = node->next;
            node->next->previous = node->previous;
        } else if (node->previous == NULL && node->next != NULL) {
            ht->table[index] = node->next;
            node->next->previous = NULL;
        } else ht->table[index] = NULL;
        free(node->key);
        free(node->value);
        free(node);
        ht->count--;
    }
}
