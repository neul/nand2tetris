#include "assembler.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 120

const int SCREEN = 16384;
const int KBD = 24576;

int main(int argc, char *argv[]) {
    hash_table* symbol_table = create_symbol_table();
    hash_table* jump_table = create_jump_table();
    hash_table* dest_table = create_dest_table();
    hash_table* comp_table = create_comp_table();
    int parsing;
    if (argc < 2) {
        printf("Filename should be given!\n");
        return 1;
    } 
    if (!is_valid_filename(argv[1])) {
        printf("Filename is not valid!\n");
        return 1;
    }
    if (!is_file_exits(argv[1])) {
        printf("File doesn't exist\n");
        return 1;
    }
    parse_file(argv[1], symbol_table);
    printf("value: %s\n", get(symbol_table, "23")->value);
    printf("value: %s\n", get(symbol_table, "23")->value);
    printf("value: %s\n", get(symbol_table, "23")->value);

    return 0;
}

hash_table* create_symbol_table() {
    hash_table* ht = create_hash_table();
    for (int i = 0; i <= 15; i++) {
        char key[4];
        char value[3];

        snprintf(key, sizeof(key), "R%d", i);
        snprintf(value, sizeof(value), "%d", i);

        add(ht, key, value);
    }
    add(ht, "SCREEN", "16384");
    add(ht, "KBD", "24576");
    add(ht, "SP", "0");
    add(ht, "LCL", "1");
    add(ht, "ARG", "2");
    add(ht, "THIS", "3");
    add(ht, "THAT", "4");
    return ht;
}

hash_table* create_jump_table() {
    hash_table* ht = create_hash_table();
    add(ht, "null", "000");
    add(ht, "JGT", "001");
    add(ht, "JEQ", "010");
    add(ht, "JGE", "011");
    add(ht, "JLT", "100");
    add(ht, "JNE", "101");
    add(ht, "JLE", "110");
    add(ht, "JMP", "111");
    return ht;
}

hash_table* create_dest_table() {
    hash_table* ht = create_hash_table();
    add(ht, "null", "000");
    add(ht, "M", "001");
    add(ht, "D", "010");
    add(ht, "DM", "011");
    add(ht, "A", "100");
    add(ht, "AM", "101");
    add(ht, "AD", "110");
    add(ht, "111", "111");
    return ht;
}

hash_table* create_comp_table() {
    hash_table* ht = create_hash_table();
    add(ht, "00", "101010");
    add(ht, "10", "111111");
    add(ht, "-10", "111010");
    add(ht, "D0", "001100");
    add(ht, "!D0", "001101");
    add(ht, "-D0", "001111");
    add(ht, "D+10", "011111");
    add(ht, "D-10", "001110");
    add(ht, "D+A0", "000010");
    add(ht, "D-A0", "010011");
    add(ht, "D&A0", "000000");
    add(ht, "D|A0", "010101");
    add(ht, "A0", "110000");
    add(ht, "!A0", "110001");
    add(ht, "-A0", "110011");
    add(ht, "A+10", "110111");
    add(ht, "A-10", "110010");
    add(ht, "A-D0", "000111");

    add(ht, "M1", "110000");
    add(ht, "!M1", "110001");
    add(ht, "-M1", "110011");
    add(ht, "M+11", "110111");
    add(ht, "M-11", "110010");
    add(ht, "D+M1", "000010");
    add(ht, "D-M1", "000011");
    add(ht, "M-D1", "000111");
    add(ht, "D&M1", "000000");
    add(ht, "D|M1", "010101");

    return ht;
}

bool is_valid_filename(char* filename) {
    char ext[5] = { '.', 'a', 's', 'm' };
    int dotInd = strlen(filename) - 4;

    if (dotInd <= 0) return false;
    for (int i = dotInd; i < strlen(filename); i++) {
        if (filename[i] != ext[i-dotInd]) return false;
    }
    return true;
}

bool is_file_exits(char *filename) {
    FILE* fp = fopen(filename, "r");
    return (fp != NULL); 
}

bool is_line_empty(char* line) {
    return (strlen(line) == 0); 
}

bool is_line_comment(char* line) {
    if (strlen(line) >= 2)  {
        return line[0] == '/' && line[1] == '/';
    }
    return false;
}

TokenLine* create_token_struct() {
    TokenLine tk = calloc(1, sizeof(TokenLine));
    
}

TokenLine* parse(char* line, hash_table* symbol_table) {
    if (line[0] == '@') {
        char* value_str = &line[1];
        char* end_ptr;
        int value = (int)strtol(value_str, &end_ptr, 10);

        if (value_str == end_ptr || *end_ptr != '\0' || *end_ptr != '/') {
            printf("not valid parse\n");
            return -1;
        }
        if (value >= 0 && value <= 32767) return -1;
        return 1;
        // variable :TODO
    }

    //strtok(line, "=");
    return -1;
}

int change_char_to_int(char* charNum) {
    int sum = 0;
    int lenNum = strlen(charNum);
    printf("len: %d\n", lenNum); 
    for (int i = 0; i < lenNum; i++) {
        int num = charNum[i] - '0';
        printf("num: %c\n", charNum[i]); 
        printf("place: %d\n", place_value(lenNum-1-i)); 
        if (i == lenNum-1) 
            sum += num;
        else 
            sum += place_value(lenNum-1-i) * num;
    }
    return sum;
}

int place_value(int place) {
    int sum = 1;
    int multp = 10;
    if (place == 0) return 1;
    for (int i = 0; i < place; i++) 
        sum *= multp;
    return sum;
}

int parse_file(char* filename, hash_table* symbol_table) {
    char line[MAX_LINE_LENGTH];
    char ext[5] = { 'h', 'a', 'c', 'k' };
    int fnLen = strlen(filename);
    int lineCount = 0;
    FILE* fptr = fopen(filename, "r");
    while(fgets(line, MAX_LINE_LENGTH, fptr)) {
        if (!is_line_empty(line) && !is_line_comment(line) && strlen(line) >= 2) {
            parse(line, symbol_table);
            // parse will return a struct based on that: add things to the table with lineC
            lineCount++;
        }
    }
    fclose(fptr);
    return 1;
}


