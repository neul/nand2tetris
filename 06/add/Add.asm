// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/06/add/Add.asm

// Computes R0 = 2 + 3  (R0 refers to RAM[0])
// https://inv.vern.cc/watch?v=9Sq94l9fYqw&list=PLrDd_kMiAuNmSb-CKWQqq9oBFN_KNMTaI&index=41
@2
D=A
@3
D=D+A
@0
M=D
